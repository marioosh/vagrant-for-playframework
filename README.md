vagrant-for-playframework
===================

A project that uses Vagrant and Puppet to create and boot a VirtualBox VM and then download and install a copy of PlayFramework. It's configured for development.  

## Notes

1. Server can take *awhile* to startup and be accessible after installation.  Give it a few minutes to go through initial startup.
2. A PostgreSQL server is installed by default but you're not required to use it.  
3. If you want to use the installed PostgreSQL server, you should provide the following information in playframework `application.conf` setup:
	1. Database Type: PostgreSQL
	2. Hostname: localhost
	2. Port: 5432
	3. Database: pgdev
	4. Username: pgdev
	5. Password: pgdev
	
    Example from version 1.2.7 :
	
        db.url=jdbc:postgresql:pgdev  
        db.driver=org.postgresql.Driver  
        db.user=pgdev  
        db.pass=pgdev    
     

4. An Apache reverse proxy is used to map port 9000 to 80. You can access the application by IP address in the browser, you no need add port after address
5. There is defined simple domain name. Application will be published in this domain. If you have to access this address from local computer you need map IP and the domain in your `/etc/hosts` file.
6. Defined PlayFramework version is download and unzipped into `/opt/play-version_number` folder. This folder is added to `$PATH`. You can use `play` command from any place you want.
7. Application should be stored into `/vagrant` folder. This folder is shared with guest machine. You can modify your code and all is changed in vagrant server automatically.
8. Provisioning take few minutes to finish.	

## Dependencies

1. [Vagrant](http://downloads.vagrantup.com/)
2. [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

##Customize

Most predefined data are in the upper section of `manifest/default.pp` file.
   
    $play_version="1.2.7" #version of framework to download and use
    $app_name = "play_app" ## application folder's name
    $app_port = "9000" ##default application port
    $app_domain = "play_app.local" ##domain mapped in apache proxy
    $db_name="pgdev" ##postgres credentials
    $db_user="pgdev" ##
    $db_pass="pgdev" ##

## Usage

	$ git clone git clone https://marioosh@bitbucket.org/marioosh/vagrant-for-playframework.git
	$ play new play_app
	$ vagrant up
	$ vagrant ssh
	$ play run

Once Application is up and running you can access it at http://192.168.33.10 or http://play_app.local (if you add this domain at /etc/hosts file).
