class must-have {
include apt
include postgresql::server

apt::ppa { "ppa:webupd8team/java": }

$play_version="1.2.7"
$play_folder="/opt"
$play_home = "${play_folder}/play-${play_version}"
$app_name = "play_app"
$app_port = "9000"
$app_domain = "play_app.local"
$db_name="pgdev"
$db_user="pgdev"
$db_pass="pgdev"

file { "sites-available.default":
  path => "/etc/apache2/sites-available/default",
  content => template('play/play.sites-available.default'),
  require => Package["apache2"],
}

exec { 'apt-get update':
  command => '/usr/bin/apt-get update',
  before => Apt::Ppa["ppa:webupd8team/java"],
}

package { ["vim",
"curl",
"bash",
"apache2",
"wget",
"unzip" ]:
  ensure => present,
  require => Exec["apt-get update"],
  before => Apt::Ppa["ppa:webupd8team/java"],
}

postgresql::db { "${db_name}":
  user     => "${db_user}",
  password => "${db_pass}",
}

package { ["oracle-java7-installer"]:
  ensure => present,
  require => Exec["apt-get update"],
}

exec { "/usr/sbin/a2enmod proxy":
  unless => "/bin/readlink -e /etc/apache2/mods-enabled/proxy.load",
  notify => Exec["reload-apache2"],
  require => File["sites-available.default"],
}

exec { "/usr/sbin/a2enmod proxy_http":
  unless => "/bin/readlink -e /etc/apache2/mods-enabled/proxy_http.load",
  notify => Exec["reload-apache2"],
  require => File["sites-available.default"],
}

exec {
  "accept_license":
  command => "echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections && echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections",
  cwd => "/home/vagrant",
  user => "vagrant",
  path    => "/usr/bin/:/bin/",
  require => Package["curl"],
  before => Package["oracle-java7-installer"],
  logoutput => true,
}


exec {
  "download_play":
  command => "/usr/bin/wget http://downloads.typesafe.com/play/${play_version}/play-${play_version}.zip -O /tmp/play-${play_version}.zip",
  logoutput => on_failure,
  creates   => "/tmp/play-${play_version}.zip",
  require   => [ Package["wget"] ]
}

exec {
  "unzip_play":
  cwd => "${play_folder}",
  command => "/usr/bin/unzip /tmp/play-${play_version}.zip",
  creates => "${play_home}",
  require => [Package["unzip"], Exec["download_play"]]
}
exec {
  "set_path":
  command => "/bin/echo \"export PATH=\$PATH:/${play_home}\" >> .bashrc"
}

exec { "reload-apache2":
  command => "/etc/init.d/apache2 reload",
  refreshonly => true,
}

exec { "force-reload-apache2":
  command => "/etc/init.d/apache2 force-reload",
  refreshonly => true,
}

# We want to make sure that Apache2 is running.
service { "apache2":
  ensure => running,
  hasstatus => true,
  hasrestart => true,
  require => Package["apache2"],
}
}

include must-have
